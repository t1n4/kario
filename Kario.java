import java.io.*;
import java.util.*;
import java.awt.*;
import javax.swing.*;

public class Kario extends JFrame {
    public Kario() {
        setTitle("itemBlock");
        setResizable(false);

        MainPanel panel = new MainPanel();
        Container contentPane = getContentPane();
        contentPane.add(panel);

        pack();
    }

    public static void main(String[] args) {
        Kario frame = new Kario();
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);
    }
}
