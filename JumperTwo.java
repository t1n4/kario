import java.applet.Applet;
import java.applet.AudioClip;

public class JumperTwo extends Sprite {

    public JumperTwo(double x, double y, String fileName, Map map) {
        super(x, y, fileName, map);
    }

    public void update() {
    }
    /**
     * アイテムを使う
     */
    public void use(Player player) {
        // プレイヤーが二段ジャンプ可能に！
        player.setJumperTwo(true);
    }
}
