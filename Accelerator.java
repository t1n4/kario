import java.applet.Applet;
import java.applet.AudioClip;

public class Accelerator extends Sprite {

    public Accelerator(double x, double y, String fileName, Map map) {
        super(x, y, fileName, map);
    }

    public void update() {
    }
    /**
     * アイテムを使う
     */
    public void use(Player player) {
        // プレイヤーのスピードがアップ！
        player.setSpeed(player.getSpeed() * 2);
    }
}
